module codeberg.org/rumpelsepp/gcat

go 1.18

require (
	codeberg.org/rumpelsepp/helpers v0.0.0-20211020091314-b9b064cf8c8a
	github.com/Fraunhofer-AISEC/penlogger v0.0.0-20210914113712-8a2b1758b080
	github.com/creack/pty v1.1.18
	github.com/gliderlabs/ssh v0.3.3
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/jedib0t/go-pretty/v6 v6.3.0
	github.com/miekg/dns v1.1.48
	github.com/pkg/sftp v1.13.4
	github.com/spf13/cobra v1.4.0
	github.com/vishvananda/netlink v1.2.0-beta
	goftp.io/server/v2 v2.0.0
	golang.org/x/exp v0.0.0-20220328175248-053ad81199eb
	golang.org/x/net v0.0.0-20220403103023-749bd193bc2b
	golang.org/x/sys v0.0.0-20220405052023-b1e9470b6e64
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/vishvananda/netns v0.0.0-20211101163701-50045581ed74 // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3 // indirect
	golang.org/x/tools v0.1.10 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
